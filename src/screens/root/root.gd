extends Node

var current_screen
var version = "v0.2.1"

var label_fps = -1
var label_version = ""

var screens = {
        #"main_menu": preload("res://screens/main_menu/main_menu.tscn"),
        "game": preload("res://screens/game/game.tscn"),
        #"options": preload("res://screens/options/options.tscn"),
    }

func _ready():
    label_fps = find_node("fps")
    label_fps.visible = false
    label_version = find_node("version")
    label_version.text = version

    current_screen = find_node("screen")
    _load_screen("game")

func _input(event):
    if event.is_action_pressed("game_quit"):
        _load_screen("main_menu")
    elif event.is_action_pressed("display_debug"):
        label_fps.set_visible(!label_fps.visible)

func _process(delta):
    label_fps.set_text("FPS: %d" % Performance.get_monitor(Performance.TIME_FPS))

func _load_screen(name):
    if name in screens:
        var old_screen = null
        if current_screen.get_child_count() > 0:
            old_screen = current_screen.get_child(0)
            current_screen.remove_child(old_screen)

        var new_screen = screens[name].instance()
        new_screen.connect("next_screen", self, "_load_screen")
        current_screen.add_child(new_screen)
    else:
        print("[ERROR] Cannot load screen: ", name)
