extends "../abstract_screen.gd"

enum players {HUMAN, AI}
enum states {PLAY, GAME_OVER}

var BOARD_SIZE = 3

var board_sprite

var board = []
var cells_remaining = BOARD_SIZE*BOARD_SIZE # 3x3 regular tic tac toe board
var current_player = players.HUMAN
var winner = null
var state = states.PLAY
var color_human = Color("#4b7bec")
var color_ai = Color("#fc5c65")


# ============
#    ENGINE
# ============
func _ready():
    board_sprite = find_node("board")
    for i in range(BOARD_SIZE):
        board.append([])
        board[i].resize(BOARD_SIZE)

func _draw():
    for y in range(len(board)):
        for x in range(len(board[y])):
            var color = Color()
            if board[x][y] == players.HUMAN:
                color = color_human
            elif board[x][y] == players.AI:
                color = color_ai
            else:
                continue

            var pos = Vector2(x*200+340,y*200+60)
            var offset_pos = Vector2(3,3)
            var offset_size = Vector2(6,6)
            var size = Vector2(200,200)
            var rect = Rect2(pos+offset_pos, size-offset_size)

            draw_rect(rect, color)

func _input(event):
    if not event.is_action_pressed("action"):
        return

    if state == states.PLAY:
        game_loop()
    else:
        # game ended
        pass


# ==========
#    GAME
# ==========
# let the human play a turn, called on input event
# after the human made a valid turn, let the ai play a turn
# check if the player won after his turn, check if the ai won after their turn
# if someone won or there is no more empty cells, the round is over
func game_loop():
    play_turn()

    check_board_for_game_over()
    if winner != null or cells_remaining == 0:
        game_over()

# play a turn depending on whose turn it is
func play_turn():
    if current_player == players.AI:
        play_ai_turn()
    elif current_player == players.HUMAN:
        play_human_turn()
    else:
        print("[ERROR] invalid player: " + str(current_player))

# the ai places a marker on a random position
# TODO bot can only detect rows to block, add columns and diagonals
func play_ai_turn():
    var x
    var y
    var target = two_in_a_row()
    if target != Vector2(-1,-1):
        x = target.x
        y = target.y
    else:
        x = int(rand_range(0, BOARD_SIZE))
        y = int(rand_range(0, BOARD_SIZE))
        # repeat until we have a free cell
        while board[x][y] != null:
            x = int(rand_range(0, BOARD_SIZE))
            y = int(rand_range(0, BOARD_SIZE))
    update_cell(x, y, players.AI)
    current_player = players.HUMAN
# try to place the human marker at the clicked position
# do nothing if it's already occupied
func play_human_turn():
    var game_pos = get_tile_clicked()
    # if clicked position is valid, put the humans sign there
    if game_pos == Vector2(-1,-1):
        print("[INFO] clicked outside the board")
        return
    if board[game_pos.x][game_pos.y] != null:
        print("[INFO] field occupied")
        return

    update_cell(game_pos.x, game_pos.y, players.HUMAN)
    current_player = players.AI
    check_board_for_game_over()
    # let the AI play a turn if we didn't win yet
    if winner != null or cells_remaining == 0:
        game_over()
    else:
        play_ai_turn()


# grabs the mouse position and converts it to ingame coordinates
# board-dims/pos: (600,600), (340,60)
func get_tile_clicked():
    var mouse_pos = get_global_mouse_position()
    var game_pos = Vector2()

    var board_size = board_sprite.texture.get_size()
    # converting position from center to top left
    var board_pos = board_sprite.position-board_size/2

    # convert mouse position to game coordinates
    game_pos.x = (mouse_pos.x-board_pos.x)/(board_size.x/BOARD_SIZE)
    game_pos.y = (mouse_pos.y-board_pos.y)/(board_size.y/BOARD_SIZE)

    if not ((0 < game_pos.x and game_pos.x < BOARD_SIZE)
            and (0 < game_pos.y and game_pos.y < BOARD_SIZE)):
        return Vector2(-1,-1)
    else:
        game_pos.x = int(game_pos.x)
        game_pos.y = int(game_pos.y)

    return game_pos
# place the marker of 'player' on the cell at ('x','y')
func update_cell(x, y, player):
    board[x][y] = player
    update() # re-draw
    cells_remaining -= 1

# check if any row, column or diagonal is filled
# if a winner is found, update the 'winner' variable
func check_board_for_game_over():
    check_rows()
    check_columns()
    check_diagonals()

func check_rows():
    for x in range(BOARD_SIZE):
        var row = []
        for y in range(BOARD_SIZE):
            if board[y][x] != null:
                row.append(board[y][x])
        if row.size() == BOARD_SIZE and row[0] == row[1] and row[1] == row[2]:
            winner = row[0]
func check_columns():
    for x in range(BOARD_SIZE):
        var row = []
        for y in range(BOARD_SIZE):
            if board[x][y] != null:
                row.append(board[x][y])
        if row.size() == BOARD_SIZE and row[0] == row[1] and row[1] == row[2]:
            winner = row[0]
func check_diagonals():
    if board[0][0] == board[1][1] and board[1][1] == board[2][2] \
            or board[2][0] == board[1][1] and board[1][1] == board[0][2]:
        winner = board[1][1]

func two_in_a_row():
    for x in range(BOARD_SIZE):
        var row = []
        for y in range(BOARD_SIZE):
            if board[y][x] == players.HUMAN:
                row.append(Vector2(x,y))
        if row.size() == 2:
            var rows = range(BOARD_SIZE)
            for cell in row:
                rows.erase(int(cell.y))
            var xx = rows[0]
            var yy = row[0].x
            if board[xx][yy] == null:
                return Vector2(xx, yy)
    return Vector2(-1,-1)

# show the game over popup
func game_over():
    # update the game state
    state = states.GAME_OVER

    # TODO: play an animation instead!
    # wait before showing the popup, prevents accidental clicks
    var t = Timer.new()
    t.set_wait_time(1)
    t.set_one_shot(true)
    self.add_child(t)
    t.start()
    yield(t, "timeout")
    t.queue_free()

    # figure out what text to use and show the popup
    var text = "Lorem Ipsum"
    if winner == null:
        text = "It's a tie!"
    elif winner == players.AI:
        text = "Shame, the AI won!"
    else:
        text = "You won! Awesome!"
    $game_over_dialog/vbox/label.text = text

    $game_over_dialog.popup()

# show the menu, duh
func show_menu():
    get_tree().quit()

# ===============
#    CALLBACKS
# ===============
func _on_exit_pressed():
    get_tree().quit()
func _on_play_again_pressed():
    var error = get_tree().reload_current_scene()
    if error != OK:
        print("[ERROR] Could not reload current scene")
        get_tree().quit()
